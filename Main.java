import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

/**
 * Main
 */
public class Main {

    public static void main(String[] args) {
        Date currentDate = new Date();

        // HANDS ON #1

        // method
        System.out.println(formatDate(currentDate));

        // lambda
        Function<Date, String> formatDateLambda = date -> {
            SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY");
            return sdf.format(date);
        };
        System.out.println(formatDateLambda.apply(currentDate));

        List<Date> dates = Arrays.asList(new Date(), new Date(), new Date());

        List<String> formattedDates = dates.stream().map(date -> (new SimpleDateFormat("DD-MM-YYYY")).format(date))
                .toList();

        formattedDates.forEach(formattedDate -> System.out.println(formattedDate));

        // HANDS ON #2

        /*
         * Method reference adalah saat menggunakan method sebagai value yang me-return
         * code block dan parameter-parameternya.
         */

        // contoh

        dates.forEach(System.out::println);

    }

    private static String formatDate(Date someDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY");
        return sdf.format(someDate);
    }

}